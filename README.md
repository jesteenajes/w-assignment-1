Django REST framework
-------------------------------
Django REST framework is a powerful and flexible toolkit for building Web APIs.

Some reasons we might want to use REST framework:

The Web browsable API is a huge usability win for your developers.

Authentication policies including packages for OAuth1a and OAuth2.

Serialization that supports both ORM and non-ORM data sources.

Customizable all the way down - just use regular function-based views if you don't need the more powerful features.

Extensive documentation, and great community support.

Used and trusted by internationally recognised companies including Mozilla, Red Hat, Heroku, and Eventbrite.

Requirements
--------------------------
REST framework requires the following:

Python (3.5, 3.6, 3.7, 3.8, 3.9)
Django (2.2, 3.0, 3.1)


Asynchronous
--------------------------
Why should you use asynchronous views?

Async views are a great way to handle concurrency in an application and provide many advantages over their synchronous view counterpart. They are far more efficient when it comes to handling resources and blocking I/O tasks, and they are far easier to write than sync views as resource efficiency eliminates the number of tasks that have to run simultaneously. An example of this would be to use async/await to schedule multiple tasks in a single thread.

When using an asynchronous task, the web application running in the browser would not have to wait for the response to complete. Instead, it can perform other tasks and complete the async request when there is a response from the server.

Prerequisites

Python 3.6 and above.
Django 3.1 and above.

Installation
------------------------------
Install using pip, including any optional packages you want...

pip install djangorestframework

pip install pymysql       # Database.

pip install django-cors-headers  # A Django App that adds Cross-Origin Resource Sharing (CORS) headers to   
responses. This allows in-browser requests to your Django application from other origins.


Running the Application
--------------------------

To use - please clone the repository and then set up your virtual environment using the requirements.txt file with pip and virtualenv. You can achieve this with:

git clone https://gitlab.com/jesteenajes/w-assignment-1.git

cd w-assignment-1

docker-compose build --no-cache

docker-compose up

Open the URL http://localhost:8080/ to access the application.