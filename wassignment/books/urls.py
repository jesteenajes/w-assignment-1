from django.urls import path
from books import views 

urlpatterns = [
    path('api/books/', views.books, name="books"),
    path('api/books/register/', views.register_book, name="register"),
]