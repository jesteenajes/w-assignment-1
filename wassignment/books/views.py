import asyncio
import requests
import json

from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser
from django.http import HttpResponse
from rest_framework import status
from django.conf import settings

from books.models import Book
from books.serializers import BookSerializer
from rest_framework.decorators import api_view

API_ENDPOINT = settings.API_ENDPOINT
book_titles = ["title1", "title2"]


@api_view(['POST'])
def register_book(request):
    ''' Function to register a new record'''
    book_data = JSONParser().parse(request)
    try:
        asyncio.run(function_async(book_data))
    except:
        return HttpResponse('Failed', status=status.HTTP_400_BAD_REQUEST)
    return HttpResponse('Successfully added', status=status.HTTP_201_CREATED)


async def function_async(book_data):
    '''Schedule the execution'''
    async_task1 = asyncio.create_task(save_record_in_db(book_data))
    async_task2 = asyncio.create_task(
        append_title_to_list(book_data['title']))


async def save_record_in_db(book_data):
    '''Call api to save db'''
    headers = {'Content-Type': 'application/json'}
    payload = {
        "title": book_data.get("title", ""),
        "description": book_data.get("description", ""),
        "published": book_data.get("published", False)
    }
    response = requests.request("POST",
                                url=API_ENDPOINT, data=json.dumps(payload), headers=headers)
    return response.text


async def append_title_to_list(title):
    '''Append to list defined '''
    book_titles.append(title)
    return True


@api_view(['GET', 'POST'])
def books(request):
    '''Fetch all records and create new record'''
    if request.method == 'GET':
        books = Book.objects.all()

        title = request.GET.get('title', None)
        if title is not None:
            books = books.filter(title__icontains=title)

        books_serializer = BookSerializer(books, many=True)
        return JsonResponse(books_serializer.data, safe=False)
        # 'safe=False' for objects serialization, If it's set to False,
        # any object can be passed for serialization (otherwise only dict instances are allowed).
    elif request.method == 'POST':
        ''' Function to register a new record'''
        book_data = JSONParser().parse(request)
        book_serializer = BookSerializer(data=book_data)
        if book_serializer.is_valid():
            book_serializer.save()
            return HttpResponse('Successfully added', status=status.HTTP_201_CREATED)
        return HttpResponse('Failed', status=status.HTTP_400_BAD_REQUEST)
