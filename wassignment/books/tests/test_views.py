from .test_setup import TestSetUp


class TestViews(TestSetUp):
    def test_book_cannot_register_with_no_data(self):
        res = self.client.post(self.register_url)
        self.assertEqual(res.status_code, 400)

    def test_book_can_register(self):
        res = self.client.post(
            self.register_url, self.book_data, format="json")
        self.assertEqual(res.status_code, 201)
