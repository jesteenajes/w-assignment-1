from rest_framework.test import APITestCase, APIClient
from django.urls import reverse


class TestSetUp(APITestCase):

    def setUp(self):
        self.register_url = reverse('register')

        self.book_data = {
            'title': "test_title",
            'description': "test_desc",
            'published': True
        }
        return super().setUp()

    def tearDown(self):
        return super().tearDown()
