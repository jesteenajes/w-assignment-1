FROM python:3.7-alpine
COPY ./requirements.txt /requirements.txt
RUN pip install -r /requirements.txt
COPY ./wassignment app
WORKDIR /app
RUN chmod u+x entrypoint.sh
ENTRYPOINT ["./entrypoint.sh"]
